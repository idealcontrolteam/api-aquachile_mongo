import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateMeasurementDTO } from './dto/measurement.dto';
import { MeasurementService } from './measurement.service';
// import { AuthGuard } from 'src/shared/auth.guard';
// import { Measurement } from './interfaces/measurement.interface';
import * as moment from 'moment';
import { UserService } from '../user/user.service'
import { GLOBAL_URL } from './../global'
import * as bcrypt from 'bcryptjs';
import { ZoneService } from '../zone/zone.service'
import { LocationService } from '../location/location.service'

@Controller(`${GLOBAL_URL}/registros/sonda`)
// //@UseGuards(new AuthGuard())
export class MeasurementController {
  constructor(private measurementService: MeasurementService,
              private userService: UserService,
              private locationService: LocationService,
              private zoneService: ZoneService) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (key != 'value' && key != 'dateTime' && key != 'active') {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };

  async validateUser(body){
    const userDB=await this.userService.login(body)
    if (!userDB || !(await bcrypt.compare(body.password, userDB.password))) {
      return false
    }
    if(userDB.code.indexOf(body.empresa) > -1){
      return true
    }else{
      return false
    }
  }

  // @Post()
  // async createMeasurement(@Res() res, @Body() body: CreateMeasurementDTO) {
  //   this.validateIds(body);

  //   const newMeasurement = await this.measurementService.createMeasurement(
  //     body,
  //   );
  //   return res.status(HttpStatus.CREATED).json({
  //     statusCode: HttpStatus.CREATED,
  //     message: 'Measurement created successfully',
  //     data: newMeasurement,
  //   });
  // }

  // @Get()
  // async getMeasurements(@Res() res) {
  //   const measurements = await this.measurementService.getMeasurements();

  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  // @Get('/:measurementId')
  // async getMeasurement(@Res() res, @Param('measurementId') measurementId) {
  //   if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Measurement id is not a valid ObjectId');
  //   }

  //   const measurement = await this.measurementService.getMeasurement(
  //     measurementId,
  //   );
  //   if (!measurement) {
  //     throw new NotFoundException('Registro not found');
  //   }

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Registro found',
  //     data: measurement,
  //   });
  // }
///:code
  @Post('/:locationId')
  async getMeasurement(@Res() res, @Param('locationId') locationId,@Body() body: any) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    let valido=await this.validateUser(body);
    let measurement=[];
    if(valido){
      measurement= await this.measurementService.getMeasurementsByLocationId(
        locationId,
      );
      if (!measurement) {
        throw new NotFoundException('Registro not found');
      }
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
    let oxd=measurement.filter(m=>m.type=="oxd");
    let oxs=measurement.filter(m=>m.type=="oxs");
    let temp=measurement.filter(m=>m.type=="temp");
    let sal=measurement.filter(m=>m.type=="sal");

    let location=await this.locationService.getLocation(locationId,body.empresa);
    let zone=await this.zoneService.getZone(location[0].zoneId);
    //console.log(zone.name)
    let data=sal.map((o,i)=>{
      return {
        "oxd":oxd!=null?oxd[i].value:[],
        "oxs":oxs!=null?oxs[i].value:[],
        "temp":temp!=null?temp[i].value:[],
        "sal":sal!=null?sal[i].value:[],
        //"locationId":o.locationId,
        "dateTime":o.dateTime
      }
    })
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Registro found',
      data: data,
    });
  }
  

//xy/:code/:from/:to
  @Post('/xy/:locationId/:fini/:ffin')
  async getMeasurementByLocation_allfilteredXY(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
    @Body() body: any
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }
    let valido=await this.validateUser(body);
    let measurements=[];
    if(valido){
      measurements = await this.measurementService.getMeasurementByLocationallfilteredXY(
        locationId,
        fini,
        ffin,
      );
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
    let oxd=measurements.filter(m=>m.type=="oxd");
    let oxs=measurements.filter(m=>m.type=="oxs");
    let temp=measurements.filter(m=>m.type=="temp");
    let sal=measurements.filter(m=>m.type=="sal");

    let data=sal.map((o,i)=>{
      return {
        "oxd":oxd!=null?oxd[i].y:[],
        "oxs":oxs!=null?oxs[i].y:[],
        "temp":temp!=null?temp[i].y:[],
        "sal":sal!=null?sal[i].y:[],
        "locationId":o.locationId,
        "x":o.x
      }
    })
    
    let msg =
      measurements.length == 0
        ? 'Registros not found'
        : 'Registros fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: data,
      count: measurements.length,
    });
  }

  @Post('/:locationId/:fini/:ffin')
  async getMeasurementByLocation_allfiltered(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
    @Body() body: any,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    let valido=await this.validateUser(body);
    let measurements=[];
    // console.log(fini+"T00:00:00.00Z")
    // console.log(ffin+"T23:55:00.00Z")
    if(valido){
      measurements = await this.measurementService.getMeasurementByLocationallfilteredXY(
        locationId,
        fini+"T00:00:00.00Z",
        ffin+"T23:55:00.00Z",
      );
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
    let oxd=measurements.filter(m=>m.type=="oxd");
    let oxs=measurements.filter(m=>m.type=="oxs");
    let temp=measurements.filter(m=>m.type=="temp");
    let sal=measurements.filter(m=>m.type=="sal");

    let location=await this.locationService.getLocation(locationId,body.empresa);
    //console.log(zone.name)
    let data=sal.map((o,i)=>{
      return {
        "oxd":oxd!=null?oxd[i].y:[],
        "oxs":oxs!=null?oxs[i].y:[],
        "temp":temp!=null?temp[i].y:[],
        "sal":sal!=null?sal[i].y:[],
        //"locationId":o.locationId,
        "dateTime":moment(o.x).format('YYYY-MM-DDTHH:mm:ss')
      }
    })
    
    let array={
      _id:locationId,
      name:location[0].name,
      active:location[0].active,
      registros:data,
    }
    //console.log(array)
    
    let msg =
      measurements.length == 0
        ? 'Registros not found'
        : 'Registros fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: array,
      count: data.length,
    });
  }
  // @Get('/location/:locationId/:fini/:ffin')
  // async getMeasurementByLocationfiltered(
  //   @Res() res,
  //   @Param('locationId') locationId,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {
  //   if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Location id is not a valid ObjectId');
  //   }

  //   const measurements = await this.measurementService.getMeasurementByLocationfiltered(
  //     locationId,
  //     fini,
  //     ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  // @Get('/tag/:tagId/:fini/:ffin')
  // async getMeasurementByTagfiltered(
  //   @Res() res,
  //   @Param('tagId') tagId,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {
  //   if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Tag id is not a valid ObjectId');
  //   }

  //   const measurements = await this.measurementService.getMeasurementByTagfiltered(
  //     tagId,
  //     fini,
  //     ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //       //---------------------Aqui


  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  // @Get('/xy/tag/:tagId/:fini/:ffin')
  // async getMeasurementByTagfilteredXY(
  //   @Res() res,
  //   @Param('tagId') tagId,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {
  //   if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Tag id is not a valid ObjectId');
  //   }

  //   const measurements = await this.measurementService.getMeasurementByTagfilteredXY(
  //     tagId,
  //     fini,
  //     ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   let prom="";
  //   let max="";
  //   let min="";
  //   let ultimo_valor="";
  //   let ultima_fecha="";
  //   if(measurements.length!=0){
  //     prom=((measurements.reduce((a, b) => +a + +b.y, 0)/measurements.length)).toFixed(1);
  //     max=measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
  //     min=measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
  //     ultimo_valor=measurements[measurements.length-1].y;
  //     ultima_fecha=moment(measurements[measurements.length-1].x).format('YYYY-MM-DD HH:mm:ss');
  //   }


  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //     prom:prom,
  //     max:max,
  //     min:min,
  //     ultimo_valor:ultimo_valor,
  //     ultima_fecha:ultima_fecha,
  //   });
  // }

  // @Get('/xy/tag/active/:tagId/:fini/:ffin')
  // async getMeasurementByTagfilteredXYActive(
  //   @Res() res,
  //   @Param('tagId') tagId,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {
  //   if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Tag id is not a valid ObjectId');
  //   }

  //   const measurements = await this.measurementService.getMeasurementByTagfilteredXYActive(
  //     tagId,
  //     fini,
  //     ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   let prom="";
  //   let max="";
  //   let min="";
  //   let ultimo_valor="";
  //   let ultima_fecha="";
  //   if(measurements.length!=0){
  //     prom=((measurements.reduce((a, b) => +a + +b.y, 0)/measurements.length)).toFixed(1);
  //     max=measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
  //     min=measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
  //     ultimo_valor=measurements[measurements.length-1].y;
  //     ultima_fecha=moment(measurements[measurements.length-1].x).format('YYYY-MM-DD HH:mm:ss');
  //   }


  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //     prom:prom,
  //     max:max,
  //     min:min,
  //     ultimo_valor:ultimo_valor,
  //     ultima_fecha:ultima_fecha,
  //   });
  // }



  // @Get('/sensor/:sensorId/:fini/:ffin')
  // async getMeasurementBySensorfiltered(
  //   @Res() res,
  //   @Param('sensorId') sensorId,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {
  //   if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Sensor id is not a valid ObjectId');
  //   }

  //   const measurements = await this.measurementService.getMeasurementBySensorfiltered(
  //     sensorId,
  //     fini,
  //     ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  

  

  // @Get('/xy/location/:locationId/:fini/:ffin')
  // async getMeasurementByLocationfilteredXY(
  //   @Res() res,
  //   @Param('locationId') locationId,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {
  //   if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Location id is not a valid ObjectId');
  //   }

  //   const measurements = await this.measurementService.getMeasurementByLocationfilteredXY(
  //     locationId,
  //     fini,
  //     ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  // @Put('/:measurementId')
  // async updateMeasurement(
  //   @Res() res,
  //   @Body() body: CreateMeasurementDTO,
  //   @Param('measurementId') measurementId,
  // ) {
  //   if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Measurement id is not a valid ObjectId');
  //   }

  //   this.validateIds(body);

  //   const updatedMeasurement = await this.measurementService.updateMeasurement(
  //     measurementId,
  //     body,
  //   );
  //   if (!updatedMeasurement) {
  //     throw new NotFoundException('Measurement not updated');
  //   }

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Measurement updated',
  //     data: updatedMeasurement,
  //   });
  // }

  // @Delete('/:measurementId')
  // async deleteMeasurement(@Res() res, @Param('measurementId') measurementId) {
  //   const deletedMeasurement = await this.measurementService.deleteMeasurement(
  //     measurementId,
  //   );

  //   if (!deletedMeasurement) {
  //     throw new NotFoundException('Registro not found');
  //   }
  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Measurement deleted',
  //     data: deletedMeasurement,
  //   });
  // }
}
