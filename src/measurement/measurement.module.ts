import { Module } from '@nestjs/common';
import { MeasurementController } from './measurement.controller';
import { MeasurementService } from './measurement.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementSchema } from './schemas/measurement.schema';
import { LocationSchema } from '../location/schemas/location.schema'
import { LocationService } from 'src/location/location.service';
import { ZoneSchema } from '../zone/schemas/zone.schema'
import { ZoneService } from 'src/zone/zone.service';
import { UserSchema } from '../user/schemas/user.schema'
import { UserService } from '../user/user.service'
//import { ViewsModule } from '../views/views.module';
//import {ViewsService} from '../views/views.service';


@Module({
  imports: [
    //ViewsModule,
    MongooseModule.forFeature([
      {
        name: 'Measurement',
        schema: MeasurementSchema,
        collection: 'measurement',
      },
      { name: 'User', schema: UserSchema, collection: 'user' },
      { name: 'Location', schema: LocationSchema, collection: 'location' },
      { name: 'Zone', schema: ZoneSchema, collection: 'zone' },
    ]),
  ],
  controllers: [MeasurementController],
  providers: [MeasurementService,UserService,LocationService,ZoneService
              //ViewsService
  ],
  exports: [MeasurementService],
})
export class MeasurementModule {}
