export class CreateMeasurementDTO {
  value: number;
  dateTime: Date;
  tagId: string;
  sensorId?: string;
  locationId: string;
  active: boolean;
  type: string;
}
