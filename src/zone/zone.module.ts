import { Module } from '@nestjs/common';
import { ZoneController } from './zone.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ZoneService } from './zone.service';
import { ZoneSchema } from './schemas/zone.schema';
import { LocationSchema } from '../location/schemas/location.schema'
// import { LocationModule } from '../location/location.module';
import { LocationService } from '../location/location.service';
// import { TagModule } from '../tag/tag.module';
// import { TagService } from '../tag/tag.service';

@Module({
  imports: [
    // LocationModule,
    // TagModule,
    MongooseModule.forFeature([
      { name: 'Zone', schema: ZoneSchema, collection: 'zone' },
      { name: 'Location', schema: LocationSchema, collection: 'location' },
    ]),
  ],
  controllers: [ZoneController],
  providers: [ZoneService,LocationService
    // , TagService
  ],
  exports: [ZoneService],
})
export class ZoneModule {}
