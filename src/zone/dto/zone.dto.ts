export class CreateZoneDTO {
  code?:string;
  name: string;
  active: boolean;
  workPlaceId: string;
}
