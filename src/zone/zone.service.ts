import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateZoneDTO } from './dto/zone.dto';
import { Zone } from './interfaces/zone.interface';
// import { LocationService } from '../location/location.service';
// import { Location } from '../location/interfaces/location.interface';
//import { TagService } from '../tag/tag.service';
//import { Tag } from '../tag/interfaces/tag.interface';

@Injectable()
export class ZoneService {
  constructor(
    @InjectModel('Zone') private zoneModel: Model<Zone>,
    //private locationService: LocationService,
    //private tagService: TagService,
  ) {}

  async getZones(): Promise<Zone[]> {
    const zones = await this.zoneModel.find();
    return zones;
  }

  async getZonesAll(): Promise<Zone[]> {
    const zones = await this.zoneModel.find().populate('workPlaceId');
    return zones;
  }

  // async getZoneWithTagsAndMeasurements(zoneId) {
  //   const tags = await this.tagService.getTagsByZoneId(zoneId);

  //   tags.map((tag, i) => {
  //     console.log('Position: ' + i);
  //   });

  //   return tags;
  //   /*const zones = await this.zoneModel.findById(zoneId).populate({
  //     path: 'workPlaceId'
  //   });
  //   return zones;*/
  // }

  // async getLocationsByZoneId(zoneId): Promise<Location[]> {
  //   const locations = await this.locationService.getLocationsByZoneId(zoneId);
  //   return locations;
  // }

  async getZonesByWorkPlaceId(workPlaceId): Promise<Zone[]> {
    const zones = await this.zoneModel.find({ workPlaceId: workPlaceId, active:true });
    return zones;
  }

  async getZone(id): Promise<Zone> {
    const zone = await this.zoneModel.findById(id);
    return zone;
  }

  // async getTagsByZoneId(zoneId): Promise<Tag[]> {
  //   const tags = await this.tagService.getTagsByZoneActiveId(zoneId);
  //   return tags;
  // }

  // async getOxdByZoneId(zoneId): Promise<Tag[]> {
  //   let name="oxd"
  //   const tags = await this.tagService.getOxdByZoneActiveId(zoneId);
  //   return tags;
  // }

  async createZone(createZoneDTO: CreateZoneDTO): Promise<Zone> {
    const newZone = new this.zoneModel(createZoneDTO);
    return await newZone.save();
  }

  // async deleteZone(id): Promise<Zone> {
  //   await this.locationService.deleteLocationsByZoneId(id);
  //   return await this.zoneModel.findByIdAndDelete(id);
  // }

  async updateZone(id: string, body: CreateZoneDTO): Promise<Zone> {
    return await this.zoneModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
