import { Module } from '@nestjs/common';
import { WorkPlaceController } from './work-place.controller';
import { WorkPlaceService } from './work-place.service';
import { MongooseModule } from '@nestjs/mongoose';
import { WorkPlaceSchema } from './schemas/workPlace.schema';
import { UserService } from '../user/user.service'
import { UserSchema } from '../user/schemas/user.schema'
// import { GatewayModule } from '../gateway/gateway.module';
// import { GatewayService } from '../gateway/gateway.service';
// import { ZoneService } from '../zone/zone.service';
// import { ZoneModule } from '../zone/zone.module';
// import { CompanyModule } from 'dist/company/company.module';
// import { CompanyService } from 'dist/company/company.service';

@Module({
  imports: [
    // GatewayModule,
    // ZoneModule,
    MongooseModule.forFeature([
      { name: 'WorkPlace', schema: WorkPlaceSchema, collection: 'workPlace' },
      { name: 'User', schema: UserSchema, collection: 'user' },
    ]),
  ],
  controllers: [WorkPlaceController],
  providers: [WorkPlaceService,UserService, 
    // GatewayService, ZoneService
  ],
  exports: [WorkPlaceService],
})
export class WorkPlaceModule {}
