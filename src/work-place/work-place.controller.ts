import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateWorkPlaceDTO } from './dto/workPlace.dto';
import { WorkPlaceService } from './work-place.service';
import { GLOBAL_URL } from './../global'
import * as bcrypt from 'bcryptjs';
import { UserService } from '../user/user.service';
// import { AuthGuard } from 'src/shared/auth.guard';
// import webpush from 'web-push'; 

// const webpush = require('web-push');

//const vapidKeys = webpush.generateVAPIDKeys();
const publicVapidKey ="BFD6nyuAMhu8GAE9geEWtBvFGm6NjSKV8IljbfSriqTAZ9XkyXIxTm4lSdAPPLoYU30xb5zsVHtsqZWC8c9CEA4";
const privateVapidKey ="I87tzLTPNT8sB0k3zE90cekp0w8DzLr0R3RDGoFPRAE";

// Replace with your email
// webpush.setVapidDetails('mailto:jordyp60@gmail.com', publicVapidKey, privateVapidKey);

// Prints 2 URL Safe Base64 Encoded Strings
//console.log(vapidKeys.publicKey, vapidKeys.privateKey);

@Controller(`${GLOBAL_URL}`)
//@UseGuards(new AuthGuard())
export class WorkPlaceController {
  constructor(private workPlaceService: WorkPlaceService,
              private userService:UserService,
              ) {}

  public validateIds = body => {
    try{
      Object.keys(body).map(key => {
        if (key == 'companyId' || key == 'categoryId') {
          if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
            throw new BadRequestException(`${key} is not a valid ObjectId`);
          }
        }
      });
    }catch(e){
      throw new BadRequestException(`is not a valid ObjectId`);
    }
  };

  async validateUser(body){
    const userDB=await this.userService.login(body)
    if (!userDB || !(await bcrypt.compare(body.password, userDB.password))) {
      return false
    }
    if(userDB.code.indexOf(body.empresa) > -1){
      return true
    }else{
      return false
    }
  }


  @Post("centros")
  async getWorkPlaces(@Res() res, @Body() body) {
    const workPlaces = await this.workPlaceService.getWorkPlaces(body.empresa);

    let msg =
      workPlaces.length == 0 ? 'Centro not found' : 'Centro fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Post('centros_activos')
  async getWorkPlacesActivo(@Res() res, @Body() body) {
    const workPlaces = await this.workPlaceService.getWorkPlacesActivos(body.empresa);

    let msg =
      workPlaces.length == 0 ? 'Centro not found' : 'Centro fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Post('centros_inactivos')
  async getWorkPlacesInactivo(@Res() res, @Body() body) {
    const workPlaces = await this.workPlaceService.getWorkPlacesInactivos(body.empresa);

    let msg =
      workPlaces.length == 0 ? 'Centro not found' : 'Centro fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Post('centros/:idWorkPlace')
  async getWorkPlace(@Res() res, @Param('idWorkPlace') idWorkPlace, @Body() body) {
    if (idWorkPlace==null) {
      throw new BadRequestException('WorkPlace id is not a valid ObjectId');
    }
    if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('WorkPlace id is not a valid ObjectId');
    }

    const workPlace = await this.workPlaceService.getWorkPlace(idWorkPlace,body.empresa);
    if (!workPlace) {
      throw new NotFoundException('Work place not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Work place found',
      data: workPlace,
    });
  }

  // @Get('/all')
  // async getWorkPlacesAll(@Res() res) {
  //   const workPlaces = await this.workPlaceService.getWorkPlacesAll();

  //   let msg =
  //     workPlaces.length == 0 ? 'Centro not found' : 'Centro fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: workPlaces,
  //     count: workPlaces.length,
  //   });
  // }

   // @Get('/:idCompany/company')
  // async getWorkPlacesCompany(@Res() res,
  // @Param('idCompany') idCompany,) {
  //   const workPlaces = await this.workPlaceService.getWorkPlacesByCompanyId(idCompany);

  //   let msg =
  //     workPlaces.length == 0 ? 'Centro not found' : 'Centro fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: workPlaces,
  //     count: workPlaces.length,
  //   });
  // }

  // @Get('/:code/centro')
  // async getCentro(@Res() res,
  // @Param('code') code) {
  //   const workPlaces = await this.workPlaceService.getCentro(code);

  //   let msg =
  //     workPlaces.length == 0 ? 'Centro not found' : 'Centro fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: workPlaces,
  //     count: workPlaces.length,
  //   });
  // }

  // @Get('/:nameCompany/nameCompany')
  // async getWorkPlaceCompany(@Res() res, @Param('nameCompany') nameCompany) {
  //   // if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
  //   //   throw new BadRequestException('WorkPlace id is not a valid ObjectId');
  //   // }
  //   const company = await this.companyService.getCompanyName(nameCompany);
  //   console.log(company)
  //   const workPlace = await this.workPlaceService.getWorkPlaceCompany(idWorkPlace);
  //   // if (!workPlace) {
  //   //   throw new NotFoundException('Work place not found');
  //   // }

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Work place found',
  //     data: "workPlace",
  //   });
  // }

}
