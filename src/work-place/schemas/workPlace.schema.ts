import { Schema } from 'mongoose';

export const WorkPlaceSchema = new Schema(
  {
    code: String,
    name: String,
    //ubicacion: String,
    //alertEmail: String,
    //failEmail: String,
    active: Boolean,
    endDate: String,
    //areaId: String,
    // companyId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'Company',
    //   required: true,
    // },
    // categoryId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'Category',
    //   required: true,
    // },
  },
  { versionKey: false },
);
