import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { RegisterSMA } from './interfaces/register-sma.interface';
import { CreateRegisterSMADTO } from './dto/register-sma.dto';
import { AnyMxRecord } from 'dns';

@Injectable()
export class RegisterSMAService {
  constructor(
    @InjectModel('Registros')
    private measurementCopyModel: Model<RegisterSMA>,
  ) {}

  async getMeasurements(): Promise<RegisterSMA[]> {
    const measurements = await this.measurementCopyModel.find();
    return measurements;
  }

  async getMeasurementsAll(): Promise<RegisterSMA[]> {
    const measurements = await this.measurementCopyModel
      .find()
      .populate('tagId sensorId locationId');
    return measurements;
  }

  async getMeasurementByLocationfiltered(
    dispositivoId,
    fini,
    ffin,
  ): Promise<RegisterSMA[]> {
    const measurements = await this.measurementCopyModel.find({
      dispositivoId: dispositivoId,
      dateTime: { $gte: fini+".000Z", $lte: ffin+".000Z" },
    }).sort({dateTime:-1});
    return measurements;
  }

  async getMeasurementByProcesfiltered(
    procesoId,
    fini,
    ffin,
  ): Promise<RegisterSMA[]> {
    const measurements = await this.measurementCopyModel.find({
      procesoId: procesoId,
      dateTime: { $gte: fini+".000Z", $lte: ffin+".000Z" },
    }).sort({dateTime:-1});
    return measurements;
  }

  async getMeasurementBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<any> {
    const measurements = await this.measurementCopyModel.find({
      $or: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  // async getMeasurementByLocationfiltered(
  //   id_dispositivos,
  //   fini,
  //   ffin,
  // ): Promise<any[]> {
  //   let measurements = await this.measurementCopyModel.find({
  //     id_dispositivos: id_dispositivos,
  //     fecha_registros: { $gte: fini, $lt: ffin },
  //   });
  //   return measurements.map(d=>{
  //     var fecha=new Date(d.fecha_registros);
  //     var zona_horaria=new Date(d.fecha_registros).getTimezoneOffset();
  //     zona_horaria=zona_horaria/60;
  //     let f=fecha.setHours(fecha.getHours()+zona_horaria);
  //     return {
  //       "_id": d._id,
  //       "code": d.code,
  //       "temp": d.temp,
  //       "sal": d.sal,
  //       "oxs": d.oxs,
  //       "oxd": d.oxd,
  //       "fecha_registros": f,
  //       "id_dispositivos": d.id_dispositivos
  //     }
  //   });
  // }

  async getMeasurement(id): Promise<RegisterSMA> {
    const measurement = await this.measurementCopyModel.findById(id);
    return measurement;
  }

  async getMeasurement_dispositivo(id): Promise<any[]> {
    const measurement = await this.measurementCopyModel.find({  dispositivoId: id }).sort({dateTime: -1}).limit(1);
    return measurement;
  }

  async getMeasurement_proceso(id): Promise<any[]> {
    const measurement = await this.measurementCopyModel.find({  procesoId: id }).sort({dateTime: -1}).limit(1);
    return measurement;
  }

  async getMeasurementsByLocationId(locationId): Promise<RegisterSMA[]> {
    const measurements = await this.measurementCopyModel.find({
      locationId: locationId,
    });
    return measurements;
  }

  async getMeasurementsBySensorId(sensorId): Promise<RegisterSMA[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
    });
    return measurements;
  }

  async getMeasurementsByTagId(tagId): Promise<RegisterSMA[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
    });
    return measurements;
  }

  async createMeasurement(
    createRegisterSMADTO: CreateRegisterSMADTO,
  ): Promise<RegisterSMA> {
    const newMeasurement = new this.measurementCopyModel(
      createRegisterSMADTO,
    );
    return await newMeasurement.save();
  }

  async deleteMeasurement(id): Promise<RegisterSMA> {
    return await this.measurementCopyModel.findByIdAndDelete(id);
  }

  async updateMeasurement(
    id: string,
    body: any,
  ): Promise<RegisterSMA> {
    return await this.measurementCopyModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }

}
