import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateRegisterSMADTO } from './dto/register-sma.dto';
import { RegisterSMAService } from './register-sma.service';
import { GLOBAL_URL } from './../global';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcryptjs';

@Controller(`${GLOBAL_URL}/registros_sma`)
//@UseGuards(new AuthGuard())
export class RegisterSMAController {
  constructor(private measurementService: RegisterSMAService,
              private userService: UserService) {}

  async validateUser(body){
    const userDB=await this.userService.login(body)
    // console.log(userDB)
    if (!userDB || !(await bcrypt.compare(body.password, userDB.password))) {
      return false
    }
    if(userDB.code.indexOf(body.empresa) > -1){
      return true
    }else{
      return false
    }
  }

  @Post('/sonda/:dispositivoId')
  async getMeasurement_dispositivo(
    @Res() res, @Param('dispositivoId') dispositivoId,@Body() body: any) {
    
    let measurement=[]
    let valido=await this.validateUser(body);
    if(valido){
      measurement = await this.measurementService.getMeasurement_dispositivo(
        dispositivoId,
      );
      if (!measurement) {
        throw new NotFoundException('Measurement not found');
      }
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement found',
      data: measurement,
    });
  }

  @Post('/proceso/:procesoId')
  async getMeasurement_proceso(
    @Res() res, @Param('procesoId') procesoId,@Body() body: any) {
    let valido=await this.validateUser(body);
    let measurement=[]
    if(valido){
      measurement = await this.measurementService.getMeasurement_proceso(
        procesoId,
      );
      if (!measurement) {
        throw new NotFoundException('Measurement not found');
      }
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
   

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement found',
      data: measurement,
    });
  }

  @Post('/sonda/:dispositivoId/:fini/:ffin')
  async getMeasurementByTagfiltered(
    @Res() res,
    @Param('dispositivoId') dispositivoId,
    @Param('fini') fini,
    @Param('ffin') ffin,
    @Body() body: any
  ) {
    let valido=await this.validateUser(body);
    let measurements=[]
    if(valido){
      measurements = await this.measurementService.getMeasurementByLocationfiltered(
        dispositivoId,
        fini,
        ffin,
      );
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
    let msg =
        measurements.length == 0
          ? 'Registros not found'
          : 'Registros fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }
  //getMeasurementByProcesfiltered
  @Post('/proceso/:procesoId/:fini/:ffin')
  async getMeasurementByProcesfiltered(
    @Res() res,
    @Param('procesoId') procesoId,
    @Param('fini') fini,
    @Param('ffin') ffin,
    @Body() body: any
  ) {
    let valido=await this.validateUser(body)
    let measurements=[]
    if(valido){
      measurements = await this.measurementService.getMeasurementByProcesfiltered(
        procesoId,
        fini,
        ffin,
      );
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
    
    let msg =
      measurements.length == 0
        ? 'Registros not found'
        : 'Registros fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  // @Get('/tag/:tagId/:fini/:ffin')
  // async getMeasurementByTagfiltered(
  //   @Res() res,
  //   @Param('tagId') tagId,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {
  //   if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Tag id is not a valid ObjectId');
  //   }

  //   const measurements = await this.measurementService.getMeasurementByTagfiltered(
  //     tagId,
  //     fini,
  //     ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  // @Post('/sensor/array')
  // async getMeasurementBySensorfiltered(
  //   @Res() res,
  //   // @Param('sensorId') sensorId,
  //   // @Param('fini') fini,
  //   // @Param('ffin') ffin,
  //   @Body() body
  // ) {
  //   // if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
  //   //   throw new BadRequestException('Sensor id is not a valid ObjectId');
  //   // }

  //   const measurements = await this.measurementService.getMeasurementBySensorfiltered(
  //     body.ids,
  //     body.fini,
  //     body.ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  // @Get('/location/:id_dispositivos/:fini/:ffin')
  // async getMeasurementByLocationfiltered(
  //   @Res() res,
  //   @Param('id_dispositivos') id_dispositivos,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {
  //   if (!id_dispositivos.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Location id is not a valid ObjectId');
  //   }

  //   const measurements = await this.measurementService.getMeasurementByLocationfiltered(
  //     id_dispositivos,
  //     fini,
  //     ffin,
  //   );
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  // @Put('/:measurementId')
  // async updateMeasurement(
  //   @Res() res,
  //   @Body() body: CreateRegisterSMADTO,
  //   @Param('measurementId') measurementId,
  // ) {
  //   if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Measurement id is not a valid ObjectId');
  //   }

  //   //this.validateIds(body);

  //   const updatedMeasurement = await this.measurementService.updateMeasurement(
  //     measurementId,
  //     body,
  //   );
  //   if (!updatedMeasurement) {
  //     throw new NotFoundException('Measurement not updated');
  //   }

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Measurement updated',
  //     data: updatedMeasurement,
  //   });
  // }

  // @Delete('/:measurementId')
  // async deleteMeasurement(@Res() res, @Param('measurementId') measurementId) {
  //   const deletedMeasurement = await this.measurementService.deleteMeasurement(
  //     measurementId,
  //   );

  //   if (!deletedMeasurement) {
  //     throw new NotFoundException('Measurement not found');
  //   }
  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Measurement deleted',
  //     data: deletedMeasurement,
  //   });
  // }
}
