import { Module } from '@nestjs/common';
import { RegisterSMAController } from './register-sma.controller';
import { RegisterSMAService } from './register-sma.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RegisterSMASchema } from './schemas/register-sma.schema';
import { UserSchema } from '../user/schemas/user.schema'
import { UserService } from 'src/user/user.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Registros',
        schema: RegisterSMASchema,
        collection: 'registros',
      },
      { name: 'User', schema: UserSchema, collection: 'user' },
    ]),
  ],
  controllers: [RegisterSMAController],
  providers: [RegisterSMAService,UserService],
})
export class RegisterSMAModule {}
