import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// import { BrandModule } from './brand/brand.module';
import { MongooseModule } from '@nestjs/mongoose';
// import { ModelModule } from './model/model.module';
// import { TcpConnectionModule } from './tcp-connection/tcp-connection.module';
// import { SerialConnectionModule } from './serial-connection/serial-connection.module';
// import { GatewayModule } from './gateway/gateway.module';
// import { DeviceModule } from './device/device.module';
// import { MeasurementCopyModule } from './local_control/local_control.module';
// import { LocalRegistrosModule } from './local_registros/local_registros.module';
// import { LocalRegistrosService } from './local_registros/local_registros.service';
// import { LocalRegistrosController } from './local_registros/local_registros.controller';
// import { CategoryModule } from './category/category.module';
// import { CompanyModule } from './company/company.module';
import { WorkPlaceModule } from './work-place/work-place.module';
import { UserModule } from './user/user.module';
// import { RoleModule } from './role/role.module';
import { ZoneModule } from './zone/zone.module';
import { LocationModule } from './location/location.module';
import { LocationController } from './location/location.controller';
import { LocationService } from './location/location.service';
// import { SensorModule } from './sensor/sensor.module';
import { MeasurementModule } from './measurement/measurement.module';
// import {MeasurementController} from './measurement/measurement.controller'
import { TagModule } from './tag/tag.module';
import { RegisterSMAModule } from './register-sma/register-sma.module';
// import { TagController } from './tag/tag.controller';
// import { TagService } from './tag/tag.service';
// import { TagTypeModule } from './tag-type/tag-type.module';
// import { TagConfigControlModule } from './tag-config-control/tag-config-control.module';
// import { AlarmModule } from './alarm/alarm.module';
// import { FailModule } from './fail/fail.module';
// import { ViewsController } from './views/views.controller';
// import { ViewsModule } from './views/views.module';
// import { AreaController } from './area/area.controller';
// import { AreaModule } from './area/area.module';
// import { ActivityModule } from './activity/activity.module';
// import { CyclesController } from './cycles/cycles.controller';
// import { CyclesModule } from './cycles/cycles.module';
// import { CyclesService } from './cycles/cycles.service';
// import { EventModule } from './event/event.module';
// import { GroupController } from './group/group.controller';
// import { GroupService } from './group/group.service';
// import { GroupModule } from './group/group.module';
// import { ResourceController } from './resource/resource.controller';
// import { ResourceService } from './resource/resource.service';
// import { ResourceModule } from './resource/resource.module';
// import { ArchiveController } from './archive/archive.controller';
// import { ArchiveService } from './archive/archive.service';
// import { ArchiveModule } from './archive/archive.module';
// import { MeasurementTodayController } from './measurement-today/measurement-today.controller';
// import { MeasurementTodayService } from './measurement-today/measurement-today.service';
// import { MeasurementTodayModule } from './measurement-today/measurement-today.module';
// import { RegisterTodayController } from './register-today/register-today.controller';
// import { RegisterTodayModule } from './register-today/register-today.module';
// import { RegisterTodayService } from './register-today/register-today.service';
// import { RegisterSMAController } from './register-sma/register-sma.controller';
// import { RegisterSMAService } from './register-sma/register-sma.service';
// import { MeasurementControlController } from './measurement-control/measurement-control.controller';
// import { MeasurementControlService } from './measurement-control/measurement-control.service';
// import { MeasurementControlModule } from './measurement-control/measurement-control.module';
// import { MeasurementTodayControlController } from './measurement-today-control/measurement-today-control.controller';
// import { MeasurementTodayControlModule } from './measurement-today-control/measurement-today-control.module';
// import { MeasurementTodayControlService } from './measurement-today-control/measurement-today-control.service';
// import { StatusServerModule } from './status_server/status_server.module';
// import { StatusServerService } from './status_server/status_server.service';
// import { StatusServerController } from './status_server/status_server.controller';
// import { EfectividadDayModule } from './efectividad-day/efectividad-day.module';
// import { EfectividadDayService } from './efectividad-day/efectividad-day.service';
// import { EfectividadDayController } from './efectividad-day/efectividad-day.controller';
// import { EfectividadMonthModule } from './efectividad-month/efectividad-month.module';
// import { EfectividadMonthService } from './efectividad-month/efectividad-month.service';
// import { EfectividadMonthController } from './efectividad-month/efectividad-month.controller';
@Module({
  imports: [
    // BrandModule,    
    //MongooseModule.forRoot('mongodb://idealcontrol:Ideal_cloud281@127.0.0.1:27017/bdideal_cloud?authSource=admin', {
    MongooseModule.forRoot('mongodb://idealcontrol:Ideal_cloud281@169.53.138.44:27017/bdideal_cloud?authSource=admin', {
    //MongooseModule.forRoot('mongodb://idealcontrol:Ideal_cloud281@169.53.138.44:27017/bdbeta_ideal?authSource=admin', {
    //MongooseModule.forRoot('mongodb://127.0.0.1:27017/bdideal_cloud', {
      useNewUrlParser: true,
      //useFindAndModify: false 
    }),
    // ModelModule,
    // TcpConnectionModule,
    // SerialConnectionModule,
    // GatewayModule,
    // DeviceModule,
    // MeasurementCopyModule,
    // CategoryModule,
    // CompanyModule,
    WorkPlaceModule,
    UserModule,
    // RoleModule,
    ZoneModule,
    LocationModule,
    // SensorModule,
    MeasurementModule,
    TagModule,
    RegisterSMAModule
    // TagTypeModule,
    // TagConfigControlModule,
    // AlarmModule,
    // FailModule,
    // ViewsModule,
    // AreaModule,
    // ActivityModule,
    // EventModule,
    // GroupModule,
    // ResourceModule,
    // ArchiveModule,
    // CyclesModule,
    // MeasurementTodayModule,
    // RegisterTodayModule,
    // RegisterSMAModule,
    // MeasurementControlModule,
    // MeasurementTodayControlModule,
    // StatusServerModule,
    // EfectividadDayModule,
    // EfectividadMonthModule,
    // LocalRegistrosModule,
  ],
  controllers: [
                //AppController
                // , ViewsController, AreaController, GroupController, ResourceController, ArchiveController, CyclesController
                // , MeasurementTodayController, RegisterTodayController,RegisterSMAController,MeasurementControlController,MeasurementTodayControlController
                // , StatusServerController, EfectividadDayController, EfectividadMonthController, LocalRegistrosController
              ],
  providers: [
                //AppService
                // , GroupService, ResourceService, ArchiveService, CyclesService, MeasurementTodayService, RegisterTodayService
                // ,RegisterSMAService,MeasurementControlService,MeasurementTodayControlService, StatusServerService, EfectividadDayService
                // ,EfectividadMonthService, LocalRegistrosService
              ],
})
export class AppModule {}
