import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateLcationDTO } from './dto/location.dto';
import { LocationService } from './location.service';
import { GLOBAL_URL } from '../global';
import { UserService } from './../user/user.service'
import * as bcrypt from 'bcryptjs';
// import { TagService } from '../tag/tag.service';
// import { AuthGuard } from 'src/shared/auth.guard';

@Controller(`${GLOBAL_URL}`)
//@UseGuards(new AuthGuard())
export class LocationController {
  constructor(private locationService: LocationService,
              // private TagService: TagService
              private userService: UserService
              ) {}
  
  // @Get()
  // async getLocations(@Res() res) {
  //   const locations = await this.locationService.getLocations();

  //   let msg =
  //     locations.length == 0 ? 'Sondas not found' : 'Sondas fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: locations,
  //     count: locations.length,
  //   });
  // }

  async validateUser(body){
    const userDB=await this.userService.login(body)
    if (!userDB || !(await bcrypt.compare(body.password, userDB.password))) {
      return false
    }
    if(userDB.code.indexOf(body.empresa) > -1){
      return true
    }else{
      return false
    }
  }

  @Post('sondas_activas')
  async getLocationsActive(@Res() res,@Body() body: any) {
    //console.log(body.empresa)
    let valido=await this.validateUser(body)
    let locations=[]
    if(valido){
      locations = await this.locationService.getLocationsActive(body.empresa);
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
    

    let msg =
      locations.length == 0 ? 'Sondas not found' : 'Sondas fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: locations,
      count: locations.length,
    });
  }

  @Post('sondas/:locationId')
  async getLocation(@Res() res,@Body() body: any, @Param('locationId') locationId) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }
    let valido=await this.validateUser(body)
    let location=[]
    if(valido){
      location = await this.locationService.getLocation(locationId,body.empresa);
      if (!location) {
        throw new NotFoundException('Location not found');
      }
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Location found',
      data: location,
    });
  }

  //sondas/centro/:centro
  // @Post('sondas/centro/:workplaceId')
  // async getLocationWorkplace(@Res() res,@Body() body: any, @Param('workplaceId') workplaceId) {
  //   if (!workplaceId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('workplace id is not a valid ObjectId');
  //   }
  //   let valido=await this.validateUser(body)
  //   let location=[]
  //   if(valido){
  //     location = await this.locationService.getLocationWorkplace(workplaceId,body.empresa);
  //     if (!location) {
  //       throw new NotFoundException('Location not found');
  //     }
  //   }else{
  //     return res.status(HttpStatus.UNAUTHORIZED).json({
  //       statusCode: HttpStatus.UNAUTHORIZED,
  //       message: 'Not authorized',
  //     });
  //   }

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Location found',
  //     data: location,
  //   });
  // }

  // @Get('/all')
  // async getLocationsAll(@Res() res) {
  //   const locations = await this.locationService.getLocationsAll();

  //   let msg =
  //     locations.length == 0 ? 'Sondas not found' : 'Sondas fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: locations,
  //     count: locations.length,
  //   });
  // }

   // @Post()
  // async createLocation(@Res() res, @Body() body: CreateLcationDTO) {
  //   // if (!body.zoneId.match(/^[0-9a-fA-F]{24}$/)) {
  //   //   throw new BadRequestException('ZoneId is not a valid ObjectId');
  //   // }

  //   const newLocation = await this.locationService.createLocation(body);
  //   return res.status(HttpStatus.CREATED).json({
  //     statusCode: HttpStatus.CREATED,
  //     message: 'Location created successfully',
  //     data: newLocation,
  //   });
  // }


  // @Get('/:locationId/measurement')
  // async getMesurementsByLocationId(
  //   @Res() res,
  //   @Param('locationId') locationId,
  // ) {
  //   const measurements = await this.locationService.getMesurementsByLocationId(
  //     locationId,
  //   );

  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  // }

  // @Get('/:locationId/sensor')
  // async getSensorsByLocationId(@Res() res, @Param('locationId') locationId) {
  //   const sensors = await this.locationService.getSensorsByLocationId(
  //     locationId,
  //   );

  //   let msg = sensors.length == 0 ? 'Sensors not found' : 'Sensors fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: sensors,
  //     count: sensors.length,
  //   });
  // }

  // @Get('/:locationId/tag')
  // async getTagsByLocationId(@Res() res, @Param('locationId') locationId) {
  //   if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
  //     throw new BadRequestException('Location id is not a valid ObjectId');
  //   }

  //   const tags = await this.locationService.getTagsByLocationId(locationId);

  //   let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: tags,
  //     count: tags.length,
  //   });
  // }

 
  // @Put('/:locationId')
  // async updateGateway(
  //   @Res() res,
  //   @Body() body: CreateLcationDTO,
  //   @Param('locationId') locationId,
  // ) {

  //   const updatedLocation = await this.locationService.updateLocation(
  //     locationId,
  //     body,
  //   );
  //   if (!updatedLocation) {
  //     throw new NotFoundException('Location not updated');
  //   }

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Location updated',
  //     data: updatedLocation,
  //   });
  // }

  // @Put('/:locationId/update_tags')
  // async updateLocationTags(
  //   @Res() res,
  //   @Body() body: CreateLcationDTO,
  //   @Param('locationId') locationId,
  // ) {
  //   console.log(body)
  //   const tags = await this.locationService.getTagsByLocationId(locationId);
  //   console.log(tags.length)
  //   const updatedLocation = await this.locationService.updateLocation(
  //     locationId,
  //     body,
  //   );
  //   if (!updatedLocation) {
  //     throw new NotFoundException('Location not updated');
  //   }

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Location updated',
  //     data: updatedLocation,
  //   });
  // }

  // @Delete('/:locationId')
  // async deleteLocation(@Res() res, @Param('locationId') locationId) {
  //   const deletedLocation = await this.locationService.deleteLocation(
  //     locationId,
  //   );

  //   if (!deletedLocation) {
  //     throw new NotFoundException('Location not found');
  //   }
  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Location deleted',
  //     data: deletedLocation,
  //   });
  // }
}
