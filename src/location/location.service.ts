import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Location } from './interfaces/location.interface';
import { CreateLcationDTO } from './dto/location.dto';
// import { Measurement } from '../measurement/interfaces/measurement.interface';
// import { MeasurementService } from '../measurement/measurement.service';
// import { Sensor } from '../sensor/interfaces/sensor.interface';
// import { SensorService } from '../sensor/sensor.service';
// import { TagService } from '../tag/tag.service';
// import { Tag } from '../tag/interfaces/tag.interface';
import { from } from 'rxjs';

let new_dto=l=>{
  return{
    _id: l._id,
    name: l.name,
    active: l.active,
  }
};

@Injectable()
export class LocationService {
  constructor(
    @InjectModel('Location') private locationModel: Model<Location>,
    //private measurementService: MeasurementService,
    // private sensorService: SensorService,
    // private tagService: TagService,
  ) {}

  async getLocations(): Promise<any[]> {
    const locations = await this.locationModel.find();
    return locations.map(new_dto);
  }

  async getLocationsActive(empresa): Promise<any[]> {
    const locations = await this.locationModel.find({active:true,code:{"$regex":empresa.toString(),"$options": "i"}});
    return locations.map(new_dto);
  }

  async getLocationsAll(): Promise<any[]> {
    const locations = await this.locationModel.find().populate('zoneId');
    return locations.map(new_dto);
  }

  async getLocationsByZoneId(zoneId): Promise<any[]> {
    const locations = await this.locationModel.find({ zoneId: zoneId, active: true });
    return locations.map(new_dto);
  }

  async getLocation(id,empresa): Promise<any> {
    const location = await this.locationModel.find({_id:id, code:{"$regex":empresa.toString(),"$options": "i"}});
    return location.map(new_dto);
  }

  async getLocationWorkplace(id,empresa): Promise<any> {
    const location = await this.locationModel.find({workPlaceId:id, code:{"$regex":empresa.toString(),"$options": "i"}});
    return location.map(new_dto);
  }

  // async getTagsByLocationId(locationId): Promise<Tag[]> {
  //   const tags = await this.tagService.getTagsByLocationId(locationId);
  //   return tags;
  // }

  // async getMesurementsByLocationId(locationId): Promise<Measurement[]> {
  //   const measurements = await this.measurementService.getMeasurementsByLocationId(
  //     locationId,
  //   );
  //   return measurements;
  // }

  // async getSensorsByLocationId(locationId): Promise<Sensor[]> {
  //   const sensors = await this.sensorService.getSensorsByLocationId(locationId);
  //   return sensors;
  // }

  async createLocation(createLcationDTO: CreateLcationDTO): Promise<Location> {
    const newLocation = new this.locationModel(createLcationDTO);
    return await newLocation.save();
  }

  async deleteLocation(id): Promise<Location> {
    return await this.locationModel.findByIdAndDelete(id);
  }

  async deleteLocationsByZoneId(zoneId) {
    return await this.locationModel.deleteMany({ zoneId: zoneId });
  }

  async updateLocation(id: string, body: CreateLcationDTO): Promise<Location> {
    return await this.locationModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
