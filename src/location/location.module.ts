import { Module } from '@nestjs/common';
import { LocationController } from './location.controller';
import { LocationService } from './location.service';
import { MongooseModule } from '@nestjs/mongoose';
import { LocationSchema } from './schemas/location.schema';
// import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';
import { UserSchema } from './../user/schemas/user.schema'
// import { MeasurementModule } from '../measurement/measurement.module';
// import { MeasurementService } from '../measurement/measurement.service';
// import { SensorModule } from '../sensor/sensor.module';
// import { SensorService } from '../sensor/sensor.service';
// import { TagModule } from '../tag/tag.module';
// import { TagService } from '../tag/tag.service';

@Module({
  imports: [
    //MeasurementModule,
    // TagModule,
    MongooseModule.forFeature([
      { name: 'Location', schema: LocationSchema, collection: 'location' },
      { name: 'User', schema: UserSchema, collection: 'user' },
    ]),
  ],
  controllers: [LocationController],
  providers: [LocationService,UserService
    // , MeasurementService
    //, SensorService
    // , TagService
  ],
  exports: [LocationService,UserService],
})
export class LocationModule {}
