import { Document } from 'mongoose';

export interface Tag extends Document {
  name: string;
  shortName: string;
  unity: string;
  nameAddress: string;
  address: string;
  factor: number;
  offset: number;
  lastValue: number;
  lastValueWrite: number;
  tagTypeId: string;
  zoneId: string;
  locationId: string;
  sensorId?: string;
  active: boolean;
  dateTimeLastValue: Date;
  write: boolean;
  read: boolean;
  register: boolean;
  deviceId: string;
  alertMax: number;
  alertMin: number;
  failMax: number;
  failMin: number;
  max: number;
  min: number;
  state: number;
  prom?: string;
  workplaceId?: string;
}
